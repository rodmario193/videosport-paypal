exports.init = function(server) {
	
var paypal = require('paypal-rest-sdk');

paypal.configure({
	'mode': 'sandbox', //sandbox or live
	'client_id': 'AXdjfOwgWv55Vyw0n83aoLTxd62hA2YfxjpP-K9fUk54g2opTHX5jJABA80WRXeS9F_rr47lnIZE_hBW',
	'client_secret': 'EMvdpWHGncoCwbapbJ6ELZHQgal4ZFiT958gv9tB5CMXJlEXqC-kQ8lhz2Oqy1VhNbbJuabwwRK2LYhI'
});


// // #########################################  EJECUCION PAGO PAYPAL  #########################################

server.route({
  method: 'POST',
  path: '/ejecutar-pago',
  config: {
    handler: EjecutarPago
  }
});


function EjecutarPago(request, reply){
		
	console.log("Id de pago: " + request.payload.idpago);
	console.log("Id de pagador: " + request.payload.idpagador);
	
	var paymentId = request.payload.idpago;
	var payerId = request.payload.idpagador;
	
	var execute_payment_json = {
		"payer_id": payerId,
		"transactions": [{
			"amount": {
				"currency": "USD",
				"total": "1.00"
			}
		}]
	};
	
	paypal.payment.execute(paymentId, execute_payment_json, function (error, payment) {
		if (error) {
			console.log(error.response);
			reply({ code: 0, message: "Error en ejecucion de pago" });
		} else {
			console.log("Get Payment Response");
			console.log(JSON.stringify(payment));
			reply({ code: 1, message: "Pago ejecutado exitosamente" });
		}
	});
}

// #########################################  VERIFICAR PAGO PAYPAL  #########################################

server.route({
  method: 'POST',
  path: '/verificar-pago',
  config: {
    handler: VerificarPago
  }
});

function VerificarPago(request, reply){

	console.log("Id de pago: " + request.payload.idpago);
	var paymentId = request.payload.idpago;
	
	paypal.payment.get(paymentId, function(error, payment) {
		if(error){
			console.log(error);
			reply({ code: 0, message: "Error en verificacion de pago" });
		}else{
			console.log("Get Payment Response");
			console.log(JSON.stringify(payment));
			reply({ code: 1, message: "Informacion de pago obtenida exitosamente", state: payment.state });
		}
	});
		
}



// #########################################  AUTORIZACION PAYPAL  #########################################

server.route({
  method: 'POST',
  path: '/autorizar-pago',
  config: {
    handler: AutorizacionPayPal
  }
});

function AutorizacionPayPal(request, reply){
	var UrlAprobado = "";
	const create_payment_json = {
		"intent": "sale",
		"payer": {
			"payment_method": "paypal"
		},
		"redirect_urls": {
			"return_url": "http://localhost:8082/procesar-compra.html",
			"cancel_url": "http://localhost:8082/rechazar-compra.html"
		},
		"transactions": [{
			"item_list": {
				"items": [{
					"name": "Compra de prueba",
					"sku": "001",
					"price": "1.00",
					"currency": "USD",
					"quantity": "1"
				}]
			},
			"amount": {
				"currency": "USD",
				"total": "1.00"
			},
			"description": "Compra de prueba para servicio Video Sport"
		}]
	};
	
	paypal.payment.create(create_payment_json, function (error, payment) {
		if(error){
			console.log(error);
			reply({ code: 0, message: "Error autorizando pago en Paypal, favor intentar nuevamente" });
		}else{
			console.log(payment);
			for(let i = 0; i < payment.links.length; i++){
                if(payment.links[i].rel=="approval_url") UrlAprobado = payment.links[i].href;
            }
			if(UrlAprobado==""){
                reply({ code: 0, message: "Error autorizando pago en Paypal, favor intentar nuevamente" });
            }else{
                reply({ code: 1, url: UrlAprobado, message: "Pago pre autorizado exitosamente" });
            }
		}
	});
}


}