exports.init = function(server) {
	
var axios = require('axios');
var btoa = require('btoa');	

const AMBIENTE = "SANDBOX"; // LIVE
const DOMAIN = "https://api.sandbox.paypal.com"; // https://api.paypal.com


server.route({
  method: 'POST',
  path: '/ejecutar-acuerdo',
  config: {
    handler: ejecutarAcuerdo
  }
});



async function ejecutarAcuerdo(request, reply){
	let _intCodigo = 0;
	let _strMensaje = "";
	
	var tokenauth = request.payload.tokenauth;
	var token_acuerdo = request.payload.token_acuerdo;
	console.log("Token autorización: " + tokenauth);
	console.log("Token acuerdo: " + token_acuerdo);
	
	let _respEjecutarAcuerdo = await ejecutarAcuerdoRequest(tokenauth, token_acuerdo);
	if(_respEjecutarAcuerdo.exito){
		_intCodigo = 1;
		_strMensaje = "ok";
	}else{
		_strMensaje = _respEjecutarAcuerdo.mensaje;
	}
	
	reply({  codigo: _intCodigo, mensaje: _strMensaje });

}



// ######################################### EJECUTAR ACUERDO #########################################

async function ejecutarAcuerdoRequest(tokenauth, token_acuerdo){
	return new Promise(function(resolve, reject){
		
		var url = DOMAIN + '/v1/payments/billing-agreements/' + token_acuerdo +'/agreement-execute';
		console.log(url);
		
		var headers = {
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + tokenauth
		}
		
		axios({
			method:'post',
			url: url,
			headers: headers
		})
		.then(function (response) {
			resolve({ exito: true, mensaje: 'ok' });
		})
		.catch(function (error) {
			console.log(error.response.data);
			reject({ exito: false, mensaje: error });
		});

		

		
	});	
}



server.route({
  method: 'POST',
  path: '/suscribir-plan',
  config: {
    handler: suscripcionAPlan
  }
});


async function suscripcionAPlan(request, reply){
	let token = "";
	let idplan = "";
	let _strURLApproval = "";
	let _strURLExecute = "";
	let _intCodigo = 0;
	let _strMensaje = "";
	
	// ***** Obtener token de acceso *****
	console.log(" ***** Obtener token de autenticación ***** ");
	let _respObtenerTokenAcceso = await obtenerTokenAcceso();
	if(_respObtenerTokenAcceso.exito){
		token = _respObtenerTokenAcceso.token;
		console.log("Token: " + token);
		
		// ***** Enviar solicitud para crear plan ******
		console.log(" ***** Crear plan de suscripción ***** ");
		let _respCrearPlan = await obtenerIdPlan(token);
		if(_respCrearPlan.exito){
		
			idplan = _respCrearPlan.plan_id;
			console.log("Id Plan: " + idplan);
			
			// ****** Activar plan ******
			console.log(" ***** Activar plan de suscripción ***** ");
			let _respActivarPlan = await activarPlan(idplan, token);
			if(_respActivarPlan.exito){
				
				// ***** Crear un acuerdo *****
				console.log(" ***** Crear un acuerdo de pago y obtener URLs de aprobación y ejecución ***** ");
				let _respCrearAcuerdo = await crearAcuerdo(idplan, token);
				if(_respCrearAcuerdo.exito){
					_intCodigo = 1;
					_strURLApproval = _respCrearAcuerdo.approval_url;
					_strURLExecute = _respCrearAcuerdo.execute_url;
					_strMensaje = 'OK';
				}else{
					console.log("Error en creación de acuerdo: " + _respCrearAcuerdo.mensaje);
					_strMensaje = "Error en creación de acuerdo: " + _respCrearAcuerdo.mensaje;
				}
				
			}else{
				console.log("Error en activación de plan: " + _respActivarPlan.mensaje);
				_strMensaje = "Error en activación de plan: " + _respActivarPlan.mensaje;
			}
		
		}else{
			console.log("Error en creación de nuevo plan: " + _respCrearPlan.mensaje);
			_strMensaje = "Error en creación de nuevo plan: " + _respCrearPlan.mensaje;
		}
		
	}else{
		console.log("Error en obtención de token: " + _respObtenerTokenAcceso.mensaje);
		_strMensaje = "Error en obtención de token: " + _respObtenerTokenAcceso.mensaje;
	}
	
	reply({  codigo: _intCodigo, mensaje: _strMensaje, approval_url: _strURLApproval, execute_url: _strURLExecute, token: token });
	
}

// #########################################  CREAR ACUERDO Y OBTENER URL's de APROBACION Y EJECUCION #########################################

async function crearAcuerdo(idPlan, token){
	
	return new Promise(function(resolve, reject){

		var url = DOMAIN + '/v1/payments/billing-agreements/';
		var headers = {
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + token
		}

		var data = {
		  "name": "Suscripción VideoSport",
		  "description": "Suscripción de pago mensual.",
		  "start_date": "2019-04-01T10:00:00Z",
		  "plan":
		  {
			"id": idPlan
		  },
		  "payer":
		  {
			"payment_method": "paypal"
		  }
		}
		
		axios({
			method:'post',
			url: url,
			data: data,
			headers: headers
		})
		.then(function (response) {
			//console.log(response.data);
			var _strURLApproval = response.data.links[0].href;
			var _strURLExecute = response.data.links[1].href;
			console.log(_strURLApproval + " | " + _strURLExecute);
			resolve({ exito: true, mensaje: 'ok', approval_url: _strURLApproval, execute_url: _strURLExecute });
		})
		.catch(function (error) {
			console.log(error.response.data);
			reject({ exito: false, mensaje: error });
		});

	});
	
	
}

// #########################################  ACTIVAR PLAN  #########################################

async function activarPlan(idPlan, token){
	return new Promise(function(resolve, reject){
		
		var url = DOMAIN + '/v1/payments/billing-plans/' + idPlan + '/';
		console.log(url);

		let config = {
			headers: {
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + token
			}
		}
		
		var data = [{
			"op": "replace",
			"path": "/",
			"value": {
				"state": "ACTIVE"
			}
		}];
		
		axios.patch(url, data, config)
		.then(function (response) {
			//console.log(response.data);
			console.log("EXITO!!!");
			resolve({ exito: true });
		})
		.catch(function (error) {
			console.log(error.response.data);
			reject({ exito: false });
		});
		
	});	
}


// #########################################  CREAR PLAN / OBTENER ID NUEVO PLAN #########################################

async function obtenerIdPlan(token){
	return new Promise(function(resolve, reject){

		var url = DOMAIN + '/v1/payments/billing-plans/';
		var headers = {
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + token
		}

		var data = {
		  "name": "Plan de pago diario",
		  "description": "Plan de pago diario para VideoSports",
		  "type": "infinite",
		  "payment_definitions": [
		  {
			"name": "Pago diario",
			"type": "REGULAR",
			"frequency": "DAY",
			"frequency_interval": "1",
			"amount":
			{
			  "value": "1",
			  "currency": "USD"
			},
			"cycles": "0"
		  }
		],
		  "merchant_preferences":
		  {
			"return_url": "http://localhost:8082/procesar-suscripcion.html",
			"cancel_url": "http://localhost:8082/error-suscripcion.html",
			"auto_bill_amount": "YES",
			"initial_fail_amount_action": "CONTINUE",
			"max_fail_attempts": "0"
		  }
		}

		
		axios({
			method:'post',
			url: url,
			data: data,
			headers: headers
		})
		.then(function (response) {
			//console.log(response.data);
			resolve({ exito: true, plan_id: response.data.id });
		})
		.catch(function (error) {
			console.log(error.response.data);
			reject({ exito: false, mensaje: error });
		});

	});
}


// #########################################  OBTENER TOKEN ACCESO  #########################################

async function obtenerTokenAcceso(){
	return new Promise(function(resolve, reject){
		
		var url = DOMAIN + '/v1/oauth2/token';
		
		var client_id = '';
		var client_secret = '';
		
		if(AMBIENTE == 'SANDBOX'){
			client_id = 'AXdjfOwgWv55Vyw0n83aoLTxd62hA2YfxjpP-K9fUk54g2opTHX5jJABA80WRXeS9F_rr47lnIZE_hBW';
			client_secret = 'EMvdpWHGncoCwbapbJ6ELZHQgal4ZFiT958gv9tB5CMXJlEXqC-kQ8lhz2Oqy1VhNbbJuabwwRK2LYhI';
		}else if (AMBIENTE == 'LIVE'){
			client_id = 'AWqc1bGQCQbaeMNyE2tutvH0sIE8VxyP7L_-BQ8Pfmlx0dAFrLuDsa2RH00iESniQvYKBIQjk2AgiTKV';
			client_secret = 'ECGLvfsO3UHR53JHHqB6VpwzrwePvuvJMtPyQZucID6ws8X-u_3AbyuLios0AvIwMUcvftwxj9Sf-AuP';
		}
		
		var headers = {
			'Accept': 'application/json',
			'Accept-Language': 'en_US'
		}
		
		axios({
			method:'post',
			url: url,
			data: "grant_type=client_credentials",
			headers: headers,
			auth: {
				username: client_id, 
				password: client_secret
			}
		})
		.then(function (response) {
			//console.log(response.data);
			resolve({ exito: true, mensaje: "ok", token: response.data.access_token });
		})
		.catch(function (error) {
			//console.log(error);
			reject({ exito: false, mensaje: error, token: '' });
		});
		
	});
}











	
}