'use strict';

const Hapi = require("hapi");
var corsHeaders = require('hapi-cors-headers');
var paypalPayment = require('./paypal-payment');
var paypalSuscription = require('./paypal-suscription');


var config = {
  server: {
    host: '127.0.0.1',
    port: 3011
  }
}

var server = new Hapi.Server();
server.connection({
  host: config.server.host,
  port: config.server.port
});

server.ext('onPreResponse', corsHeaders);

server.start(function() {
  var date = new Date();
  var time = date.getHours().toString().padStart(2,"0") + ":" + 
    date.getMinutes().toString().padStart(2,"0") + ":" + 
    date.getSeconds().toString().padStart(2,"0");
  console.log(time + " | Server listening on:", server.info.uri);
  paypalPayment.init(server,config);
  paypalSuscription.init(server,config);
});




// #########################################  PRUEBA GET  #########################################

server.route({
  method: 'GET',
  path: '/test',
  config: {
    handler: testFunction
  }
});

function testFunction(request, reply){
	reply({ Code: 1, Message: "GET request completed successfully" });
}


